provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "cognizant-204808"
  region      = "us-central1"
}